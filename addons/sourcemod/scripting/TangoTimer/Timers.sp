public Action Timer_Zones(Handle H_Timer) {
	float F_PointA[3], F_PointB[3], F_Length;
	int I_ZoneGroup, I_ZoneType, I_Count;

	F_Length = ((gA_Zones.Length / 20.0) * 0.1);

	if (F_Length < 0.1) {
		F_Length = 0.1;
	}

	F_Length += 0.1;

	while (gI_ZoneTemp < gA_Zones.Length) {
		I_ZoneType = gA_Zones.Get(gI_ZoneTemp, 1);
		I_ZoneGroup = gA_Zones.Get(gI_ZoneTemp, 2);

		if (!((gI_MapInfo[I_ZoneGroup][1] == 1) && ((I_ZoneType == 2) || (I_ZoneType == 5)))) {
			for (int x = 0; x < 3; x++) {
				F_PointA[x] = gA_Zones.Get(gI_ZoneTemp, x + 3);
				F_PointB[x] = gA_Zones.Get(gI_ZoneTemp, x + 6);
			}

			zone_ZoneDraw(0, F_PointA, F_PointB, I_ZoneType, F_Length, true);
		}

		gI_ZoneTemp++
		I_Count++;

		if (gI_ZoneTemp == gA_Zones.Length) {
			gI_ZoneTemp = 0;
			break;
		}

		if (I_Count == 20) {
			I_Count = 0;
			break;
		}
	}
	
	if (gI_Direction == 0) {
		if (gI_RainbowColors[0] == 255 && gI_RainbowColors[1] != 255) {
			gI_RainbowColors[1]++;
		} else if (gI_RainbowColors[1] == 255 && gI_RainbowColors[0] != 24) {
			gI_RainbowColors[0]--;
		} else if (gI_RainbowColors[0] == 24 && gI_RainbowColors[1] == 255) {
			gI_Direction = 1;
		}
	} else {
		if (gI_RainbowColors[1] == 255 && gI_RainbowColors[0] != 255) {
			gI_RainbowColors[0]++;
		} else if (gI_RainbowColors[0] == 255 && gI_RainbowColors[1] != 24) {
			gI_RainbowColors[1]--;
		} else if (gI_RainbowColors[0] == 255 && gI_RainbowColors[1] == 24) {
			gI_Direction = 0;
		}
	}
}

public Action Timer_Second(Handle H_Timer) {
	char C_TimeLeft[64];
	int I_TimeLeft, I_CurrentTime;

	I_CurrentTime = GetTime();

	I_CurrentTime = I_CurrentTime - SEASON_EPOCH_START;

	if ((I_CurrentTime % 86400) == 0) {
		misc_NewDayFire();
	}

	GetMapTimeLeft(I_TimeLeft);
	replay_CheckBots();

	Format(C_TimeLeft, 512, "%s Time Remaining:", PLUGIN_PREFIX);

	switch (I_TimeLeft) {
		case 1797: {
			Format(C_TimeLeft, 64, "%s 30 Minutes", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 1197: {
			Format(C_TimeLeft, 64, "%s 20 Minutes", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 597: {
			Format(C_TimeLeft, 64, "%s 10 Minutes", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 297: {
			Format(C_TimeLeft, 64, "%s 5 Minutes", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 57: {
			Format(C_TimeLeft, 64, "%s 1 Minute", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 27: {
			Format(C_TimeLeft, 64, "%s 30 Seconds", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 2: {
			Format(C_TimeLeft, 64, "%s 5 Seconds", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case 0: {
			gB_ChangingMaps = true;
		} case -1: {
			Format(C_TimeLeft, 64, "%s 3 Seconds", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case -2: {
			Format(C_TimeLeft, 64, "%s 2 Seconds", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);
		} case -3: {
			Format(C_TimeLeft, 64, "%s 1 Second - Changing Maps!", C_TimeLeft);
			misc_PrintToChatAll(C_TimeLeft, true, 3);

			CS_TerminateRound(0.1, CSRoundEnd_CTWin);
		}
	}

	int I_Length = gA_SqlQueries.Length;

	if (I_Length > 0) {
		char C_buffer[512];
		Transaction T_Trans = SQL_CreateTransaction();

		for (int i = 0; i < I_Length; i++) {
			gA_SqlQueries.GetString(i, C_buffer, 512);
			T_Trans.AddQuery(C_buffer);
		}

		SQL_ExecuteTransaction(gD_Main, T_Trans, sqlQueryStackSuccess, sqlQueryStackError, _, DBPrio_Normal);

		delete gA_SqlQueries;
		gA_SqlQueries = new ArrayList(512);
	}

	for (int i = 1; i <= MaxClients; i++) {
		char C_ClanTag[64];

		if (IsValidClient(i)) {
			if (gI_TimerCurrentSeason[i] > 0) {
				Format(C_ClanTag, 512, "S - ");
			}

			Format(C_ClanTag, 512, "[%sTEMP]", C_ClanTag);

			CS_SetClientClanTag(i, C_ClanTag);

			CS_SetMVPCount(i, gI_TimerPlayerExp[i][gI_TimerCurrentSeason[i]][2]);
			SetEntProp(i, Prop_Data, "m_iDeaths", gI_TimerPlayerLevel[i]);
		}
	}
	
	for (int i = 0; i < GetMaxEntities(); i++) {
		char C_Classname[512];
		
		if (IsValidEdict(i) && IsValidEntity(i)) {
			if (GetEntityClassname(i, C_Classname, sizeof(C_Classname))) {
				if (StrContains(C_Classname, "weapon_", false) == 0) {
					if (GetEntProp(i, Prop_Send, "m_hOwnerEntity") < 1) {
						AcceptEntityInput(i, "Kill");
					}
				}
			}
		}
	}
}

public Action Timer_AddZones(Handle H_Timer, int I_Client) {
	admin_AddZoneMenu(I_Client);
}

public Action Timer_LoadClients(Handle H_Timer) {
	for (int i = 0; i <= MaxClients; i++) {
		if (IsValidClient(i)) {
			misc_DelayedTeleportClientToZone(i, 0, 0, 1);
			OnClientPostAdminCheck(i);
		}
	}
}

public Action Timer_TeleportClientZone(Handle H_Timer, DataPack D_Pack) {
	int I_Client, I_ZoneGroup, I_CheckpointID, I_Type;

	D_Pack.Reset();

	I_Client = GetClientOfUserId(D_Pack.ReadCell());
	I_ZoneGroup = D_Pack.ReadCell()
	I_CheckpointID = D_Pack.ReadCell();
	I_Type = D_Pack.ReadCell();

	if (IsValidClient(I_Client)) {
		misc_TeleportClientToZone(I_Client, I_ZoneGroup, I_CheckpointID, I_Type);
	}

	CloseHandle(D_Pack);
}

public Action Timer_RecentlyAbused(Handle H_Timer, int I_UserId) {
	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		gB_TimerRecentlyAbused[I_Client] = false;
	}
}

public Action Timer_ChangeSpec(Handle H_Timer, DataPack D_Pack) {
	int I_Client, I_Target;

	D_Pack.Reset();

	I_Client = D_Pack.ReadCell();
	I_Target = D_Pack.ReadCell();

	SetEntPropEnt(I_Client, Prop_Send, "m_hObserverTarget", I_Target);

	CloseHandle(D_Pack);
}

public Action Timer_ClearEntity(Handle H_Timer, int I_EntityIndex) {
	if (IsValidEntity(I_EntityIndex) && IsValidEdict(I_EntityIndex)) {
		if (GetEntProp(I_EntityIndex, Prop_Send, "m_hOwnerEntity") < 1) {
			RemoveEdict(I_EntityIndex);
		}
	}
}

public Action Timer_PostAdminCheck(Handle H_Timer, int I_UserId) {
	int I_Client;

	I_Client = GetClientOfUserId(I_UserId);

	if (gB_ServerLoaded) {
		if (IsValidClient(I_Client)) {
			sql_TryLoadPlayerUid(I_Client);
		}

		return Plugin_Stop;
	}

	return Plugin_Continue;
}

public Action Timer_FixPushTrigger(Handle H_Timer, int I_UserId) {
	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		misc_FixPushTrigger(I_Client, false);
	}
}

public Action Timer_BotError(Handle H_Timer) {
	char C_buffer[512];

	Format(C_buffer, 512, "%s Missing Bots! Report the Map + Data to Oscar", PLUGIN_PREFIX);
	misc_PrintToChat(0, C_buffer, true, 3, false, false, false);
}

public Action Timer_ResetAutoBot(Handle H_Timer) {
	if (gI_ReplayBotIds[0] > 0) {
		char C_ClanTag[128], C_NameTag[256], C_Time[128], C_Name[128];

		gA_ReplayCloneFrames[0] = gA_ReplayFrames[0][0].Clone();
		gI_ReplayBotTicks[0] = 0;

		if (gI_TimerReplayRecords[0][0][0] != 0) {
			misc_FormatTime(gI_TimerReplayRecords[0][0][0], C_Time, 128);
			gA_Names.GetString(gI_TimerReplayRecords[0][0][1], C_Name, 128);

			Format(C_NameTag, 256, "%s - %s", C_Time, C_Name);
		} else {
			Format(C_NameTag, 256, "No [N] Au Records");
		}

		Format(C_ClanTag, 128, "[Au Replay]");

		CS_SetClientClanTag(gI_ReplayBotIds[0], C_ClanTag);
		SetClientInfo(gI_ReplayBotIds[0], "name", C_NameTag);
	}
}

public Action Timer_CheckBotQueue(Handle H_Timer, int I_BotId) {
	for (int x = 0; x < 2; x++) {
		gI_ReplayCurrentlyReplaying[I_BotId - 1][x] = -1;
	}

	int I_UserId, I_ZoneGroup, I_Style;
	char C_ClanTag[128], C_NameTag[256];

	Format(C_ClanTag, 128, "[Replay]");
	Format(C_NameTag, 256, "Use !replay");

	CS_SetClientClanTag(gI_ReplayBotIds[I_BotId], C_ClanTag);
	SetClientInfo(gI_ReplayBotIds[I_BotId], "name", C_NameTag);

	if (gA_ReplayQueue.Length > 0) {
		I_UserId = gA_ReplayQueue.Get(0, 0);
		I_ZoneGroup = gA_ReplayQueue.Get(0, 1);
		I_Style = gA_ReplayQueue.Get(0, 2);

		replay_StartReplay(GetClientOfUserId(I_UserId), I_BotId, I_ZoneGroup, I_Style);
		gA_ReplayQueue.Erase(0);
	} else {
		TeleportEntity(gI_ReplayBotIds[I_BotId], gF_ZoneSpawns[0][0], NULL_VECTOR, view_as<float>({0.0, 0.0, 0.0}));
	}
}

public Action Timer_WelcomeMessage(Handle H_Timer, int I_UserId) {
	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		menu_WelcomeMessage(I_Client, 0);
	}
}
