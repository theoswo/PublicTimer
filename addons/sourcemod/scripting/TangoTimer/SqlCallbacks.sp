public void sqlLoadZonesCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LogLoadZonesCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	if (SQL_GetRowCount(H_Hndl) > 0) {
		int I_ZoneId, I_ZoneType, I_ZoneGroup;
		float F_PointA[3], F_PointB[3];

		zone_ZoneClear();

		while (SQL_FetchRow(H_Hndl)) {
			I_ZoneId = SQL_FetchInt(H_Hndl, 1);
			I_ZoneType = SQL_FetchInt(H_Hndl, 2);
			I_ZoneGroup = SQL_FetchInt(H_Hndl, 3);

			for (int i = 0; i < 3; i++) {
				F_PointA[i] = SQL_FetchFloat(H_Hndl, i + 4);
				F_PointB[i] = SQL_FetchFloat(H_Hndl, i + 7);
			}

			zone_ZoneCache(I_ZoneId, I_ZoneType, I_ZoneGroup, F_PointA, F_PointB);

			if (I_ZoneGroup + 1 > gI_TotalZoneGroups) {
				gI_TotalZoneGroups = I_ZoneGroup + 1;
			}
		}

		zone_EntityReload();
	}

	misc_CheckSpawnPoints();
}

public void sqlLoadMapInfoCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LoadMapInfoCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	int I_ZoneGroup;

	while (SQL_FetchRow(H_Hndl)) {
		I_ZoneGroup = SQL_FetchInt(H_Hndl, 1);

		for (int i = 0; i < 2; i++) {
			gI_MapInfo[I_ZoneGroup][i] = SQL_FetchInt(H_Hndl, i + 2);
		}
	}
}

public void sqlLoadMapRecordsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LoadMapRecordsCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char C_buffer[512];
	int I_UId, I_Time, I_Jumps, I_Style, I_ZoneGroup, I_TimeStamp, I_Length;
	float F_Sync;

	Transaction T_Trans = SQL_CreateTransaction();

	while (SQL_FetchRow(H_Hndl)) {
		F_Sync = SQL_FetchFloat(H_Hndl, 3);

		I_UId = SQL_FetchInt(H_Hndl, 1);
		I_Time = SQL_FetchInt(H_Hndl, 2);
		I_Jumps = SQL_FetchInt(H_Hndl, 4);
		I_Style = SQL_FetchInt(H_Hndl, 5);
		I_ZoneGroup = SQL_FetchInt(H_Hndl, 6);
		I_TimeStamp = SQL_FetchInt(H_Hndl, 8);

		I_Length = gA_MapRecords[I_ZoneGroup][I_Style].Length;

		if (gI_TimerPlayerRecords[0][I_ZoneGroup][I_Style] == 0) {
			gI_TimerPlayerRecords[0][I_ZoneGroup][I_Style] = I_Time;

			Format(C_buffer, 512, "SELECT * FROM `t_checkpoints` WHERE mapname = '%s' AND uid = '%i' AND style = '%i' AND zonegroup = '%i' ORDER BY checkpointid ASC", gC_CurrentMap, I_UId, I_Style, I_ZoneGroup);
			T_Trans.AddQuery(C_buffer);
		}

		gA_MapRecords[I_ZoneGroup][I_Style].Resize(I_Length + 1);

		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_Time, 0);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_UId, 1);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, RoundToFloor(F_Sync * 100.0), 2);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_Jumps, 3);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_Style, 4);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_ZoneGroup, 5);
		gA_MapRecords[I_ZoneGroup][I_Style].Set(I_Length, I_TimeStamp, 6);
	}

	SQL_ExecuteTransaction(gD_Slave, T_Trans, sqlLoadTopRecordsCheckpointsSuccess, sqlLoadTopRecordsCheckpointsError, _, DBPrio_Normal);
}

public void sqlCheckZoneCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, DataPack D_Pack) {
	if (H_Hndl == null) {
		LogError("%s (CheckZoneCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	int I_UserId, I_ZoneType, I_ZoneGroup, I_Client, I_ZoneId;
	float F_PointA[3], F_PointB[3];

	D_Pack.Reset();

	I_UserId = D_Pack.ReadCell();
	I_ZoneType = D_Pack.ReadCell();
	I_ZoneGroup = D_Pack.ReadCell();

	for (int i = 0; i < 3; i++) {
		F_PointA[i] = D_Pack.ReadFloat();
	}

	for (int i = 0; i < 3; i++) {
		F_PointB[i] = D_Pack.ReadFloat();
	}

	I_Client = GetClientOfUserId(I_UserId);

	if (SQL_FetchRow(H_Hndl)) {
		I_ZoneId = SQL_FetchInt(H_Hndl, 1);
		sql_UpdateZone(I_Client, I_ZoneId, F_PointA, F_PointB);
	} else {
		sql_InsertZone(I_Client, I_ZoneType, I_ZoneGroup, F_PointA, F_PointB);
	}

	CloseHandle(D_Pack);
}

public void sqlInsertZoneCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (InsertZoneCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	sql_LoadZones();

	int I_Client;

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		PrintToChat(I_Client, "%s Zone Inserted", PLUGIN_PREFIX);
	}
}

public void sqlUpdateZoneCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (UpdateZoneCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	sql_LoadZones();

	int I_Client;

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		PrintToChat(I_Client, "%s Zone Updated", PLUGIN_PREFIX);
	}
}

public void sqlLoadTablesCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LoadTablesCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char C_SteamId[64], C_LastName[128];
	int I_UId;

	while (SQL_FetchRow(H_Hndl)) {
		I_UId = SQL_FetchInt(H_Hndl, 0);

		SQL_FetchString(H_Hndl, 1, C_SteamId, 64);
		SQL_FetchString(H_Hndl, 2, C_LastName, 128);

		gA_SteamIds.Resize(I_UId + 1);
		gA_Names.Resize(I_UId + 1);

		gA_SteamIds.SetString(I_UId, C_SteamId);
		gA_Names.SetString(I_UId, C_LastName);
	}

	gB_ServerLoaded = true;
}

public void sqlLoadPlayerRecordsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (LoadPlayerRecordsCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char C_buffer[512];
	int I_Client, I_UId, I_Time, I_Style, I_ZoneGroup;
	Transaction T_Trans = new Transaction();

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		while (SQL_FetchRow(H_Hndl)) {
			I_UId = SQL_FetchInt(H_Hndl, 1);
			I_Time = SQL_FetchInt(H_Hndl, 2);

			I_Style = SQL_FetchInt(H_Hndl, 5);
			I_ZoneGroup = SQL_FetchInt(H_Hndl, 6);

			gI_TimerPlayerRecords[I_Client][I_ZoneGroup][I_Style] = I_Time;

			Format(C_buffer, 512, "SELECT * FROM `t_checkpoints` WHERE mapname = '%s' AND uid = '%i' AND style = '%i' AND zonegroup = '%i' ORDER BY checkpointid ASC", gC_CurrentMap, I_UId, I_Style, I_ZoneGroup);
			T_Trans.AddQuery(C_buffer);
		}
	}

	SQL_ExecuteTransaction(gD_Main, T_Trans, sqlLoadPlayerCheckpointsSuccess, sqlLoadPlayerCheckpointsError, I_UserId, DBPrio_High);
}

public void sqlMainCreateTablesError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (MainTryCreateTables) - %s", SQL_PREFIX, C_Error);
}

public void sqlQueryStackError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (QueryStack) - %s", SQL_PREFIX, C_Error);
}

public void sqlLoadPlayerCheckpointsError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (LoadPlayerCheckpointsError) - %s", SQL_PREFIX, C_Error);
}

public void sqlLoadPlayerDataError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (LoadPlayerDataError) - %s", SQL_PREFIX, C_Error);
}

public void sqlLoadTopRecordsCheckpointsError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (sqlLoadTopRecordsCheckpointsError) - %s", SQL_PREFIX, C_Error);
}

public void sqlMainCreateTablesSuccess(Database D_Database, any A_Data, int I_Queries, Handle[] H_Results, any[] A_QueryData) {
	gB_ServerLoaded = true;
}

public void sqlQueryStackSuccess(Database D_Database, any A_Data, int I_Queries, Handle[] H_Results, any[] A_QueryData) {  }

public void sqlLoadPlayerCheckpointsSuccess(Database D_Database, int I_UserId, int I_Queries, Handle[] H_Results, any[] A_QueryData) {
	int I_Client, I_CheckpointId, I_CheckpointTime, I_Style, I_ZoneGroup;

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		for (int i = 0; i < I_Queries; i++) {
			while (SQL_FetchRow(H_Results[i])) {
				I_CheckpointId = SQL_FetchInt(H_Results[i], 2);

				I_CheckpointTime = SQL_FetchInt(H_Results[i], 3);
				I_Style = SQL_FetchInt(H_Results[i], 4);
				I_ZoneGroup = SQL_FetchInt(H_Results[i], 5);

				gI_TimerPlayerCheckpointRecords[I_Client][I_ZoneGroup][I_Style][I_CheckpointId] = I_CheckpointTime;
			}
		}

		gB_TimerPlayerLoaded[I_Client] = true;
	}
}

public void sqlLoadPlayerDataSuccess(Database D_Database, int I_UserId, int I_Queries, Handle[] H_Results, any[] A_QueryData) {
	char C_buffer[512];
	int I_Client, I_Style, I_ZoneGroup, I_Setting, I_Value, I_Season, I_TotalRankedStyles;

	for (int i = 0; i < gI_TotalStyles; i++) {
		if (gI_Styles[i][Ranked]) {
			I_TotalRankedStyles++;
		}
	}

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		if (SQL_FetchRow(H_Results[0])) {
			gI_TimerCurrentTokens[I_Client] = SQL_FetchInt(H_Results[0], 6);
			gI_TimerCurrentSeason[I_Client] = SQL_FetchInt(H_Results[0], 7);

			for (int i = 0; i < 2; i++) {
				gI_TimerPlayerVip[I_Client][i] = SQL_FetchInt(H_Results[0], i + 8);
			}
		} else {
			//KickPlayer(I_Client, "Error Loading Data, Kicked for Safety");
		}

		while (SQL_FetchRow(H_Results[1])) {
			I_Style = SQL_FetchInt(H_Results[1], 2);
			I_ZoneGroup = SQL_FetchInt(H_Results[1], 3);
			I_Season = SQL_FetchInt(H_Results[1], 4);
			I_Value = SQL_FetchInt(H_Results[1], 5);

			gI_TimerPlayerAttempts[I_Client][I_Season][I_ZoneGroup][I_Style] = I_Value;
		}

		if (SQL_GetRowCount(H_Results[2]) > 0) {
			while (SQL_FetchRow(H_Results[2])) {
				I_Season = SQL_FetchInt(H_Results[2], 4);

				for (int i = 0; i < 3; i++) {
					gI_TimerPlayerExp[I_Client][I_Season][i] = SQL_FetchInt(H_Results[2], i + 1);
				}
			}
		} else {
			Format(C_buffer, 512, "INSERT INTO `t_exp` VALUES ('%i', '0', '0', '0', '0');", gI_TimerPlayerUid[I_Client]);
			gA_SqlQueries.PushString(C_buffer);
		}

		for (int i = 0; i < TOTAL_SETTINGS; i++) {
			if (SQL_FetchRow(H_Results[3])) {
				I_Setting = SQL_FetchInt(H_Results[3], 1);
				I_Value = SQL_FetchInt(H_Results[3], 2);

				gI_TimerPlayerSettings[I_Client][I_Setting] = I_Value;

				if (I_Setting == 5 && I_Value == 1) {
					gB_TimerPlayerHide[I_Client] = true;
				} else if (I_Setting == 6 && I_Value == 1) {
					gB_TimerPlayerDisablePickup[I_Client] = true;
				}
			} else {
				if (i == 0) {
					I_Value = 0;
				} else {
					if (i < 5) {
						I_Value = 1;
					} else {
						I_Value = 0;
					}
				}

				gI_TimerPlayerSettings[I_Client][i] = I_Value;

				Format(C_buffer, 512, "INSERT INTO `t_settings` VALUES ('%i', '%i', '%i');", gI_TimerPlayerUid[I_Client], i, I_Value);
				gA_SqlQueries.PushString(C_buffer);
			}
		}

		while (SQL_FetchRow(H_Results[4])) {
			//Titles
		}

		if (SQL_GetRowCount(H_Results[5]) == I_TotalRankedStyles) {
			while (SQL_FetchRow(H_Results[5])) {
				I_Style = SQL_FetchInt(H_Results[5], 1);

				if (gI_Styles[I_Style][Ranked]) {
					sql_CalculatePlayerElo(I_Client, gI_TimerPlayerUid[I_Client], I_Style, 0);
				}
			}
		} else {
			Format(C_buffer, 512, "DELETE FROM `t_cache` WHERE uid = '%i'", gI_TimerPlayerUid[I_Client]);
			gA_SqlQueries.PushString(C_buffer);

			for (int i = 0; i < gI_TotalStyles; i++) {
				if (gI_Styles[i][Ranked]) {
					Format(C_buffer, 512, "INSERT INTO `t_cache` VALUES ('%i', '%i', 0);", gI_TimerPlayerUid[I_Client], i);
					gA_SqlQueries.PushString(C_buffer);
				}
			}
		}

		while (SQL_FetchRow(H_Results[6])) {
			int I_Item = SQL_FetchInt(H_Results[6], 1);

			SQL_FetchString(H_Results[6], 2, gC_TimerPlayerPrestigeShopValues[I_Client][I_Item], 512);
		}

		if (SQL_FetchRow(H_Results[7])) {
			for (int i = 0; i < 3; i++) {
				I_Value = SQL_FetchInt(H_Results[7], i + 2);
				gI_TimerPlayerRaces[I_Client][SEASON][i] = I_Value;
			}
		} else {
			Format(C_buffer, 512, "INSERT INTO `t_race` VALUES ('%i', '%i', '0', '0', '1000')", gI_TimerPlayerUid[I_Client], SEASON);
			gA_SqlQueries.PushString(C_buffer);

			gI_TimerPlayerRaces[I_Client][SEASON][2] = 1000;
		}

		gI_TimerPlayerLevel[I_Client] = misc_CalculateLevel(gI_TimerPlayerExp[I_Client][gI_TimerCurrentSeason[I_Client]][0]);

		PrintToChat(I_Client, "%s Data Loaded", PLUGIN_PREFIX);
	}
}

public void sqlLoadTopRecordsCheckpointsSuccess(Database D_Database, any A_Data, int I_Queries, Handle[] H_Results, any[] A_QueryData) {
	int I_Style, I_ZoneGroup, I_Time, I_CheckpointId;

	for (int i = 0; i < I_Queries; i++) {
		while (SQL_FetchRow(H_Results[i])) {
			I_CheckpointId = SQL_FetchInt(H_Results[i], 2);
			I_Time = SQL_FetchInt(H_Results[i], 3);
			I_Style = SQL_FetchInt(H_Results[i], 4);
			I_ZoneGroup = SQL_FetchInt(H_Results[i], 5);

			gI_TimerPlayerCheckpointRecords[0][I_ZoneGroup][I_Style][I_CheckpointId] = I_Time;
		}
	}
}

public void sqlCheckMapInfoCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, DataPack D_Pack) {
	if (H_Hndl == null) {
		LogError("%s (CheckMapInfoCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char C_buffer[512];
	int I_Client, I_ZoneGroup, I_Linear, I_Tier;

	D_Pack.Reset();

	I_Client = GetClientOfUserId(D_Pack.ReadCell());
	I_ZoneGroup = D_Pack.ReadCell();
	I_Linear = D_Pack.ReadCell();
	I_Tier = D_Pack.ReadCell();

	if (SQL_GetRowCount(H_Hndl) > 0) {
		Format(C_buffer, 512, "UPDATE `t_mapinfo` SET maptier = '%i', mapstyle = '%i' WHERE mapname = '%s' AND zonegroup = '%i'", I_Tier, I_Linear, gC_CurrentMap, I_ZoneGroup);
	} else {
		Format(C_buffer, 512, "INSERT INTO `t_mapinfo` VALUES ('%s', '%i', '%i', '%i')", gC_CurrentMap, I_ZoneGroup, I_Tier, I_Linear);
	}

	gA_SqlQueries.PushString(C_buffer);

	gI_MapInfo[I_ZoneGroup][0] = I_Tier;
	gI_MapInfo[I_ZoneGroup][1] = I_Linear;

	if (IsValidClient(I_Client)) {
		if (gI_TimerPlayerSettings[I_Client][3] == 1) {
			PrintToChat(I_Client, "%s Map Tier: %s", PLUGIN_PREFIX, (SQL_GetRowCount(H_Hndl) > 0) ? "Updated" : "Inserted");
		}
	}
}

public void sqlTryLoadPlayerUidCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (TryLoadPlayerUidCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char C_buffer[512], C_LastName[128], C_FormattedName[256];
	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		GetClientName(I_Client, C_LastName, 128);
		SQL_EscapeString(gD_Slave, C_LastName, C_FormattedName, 256);

		if (SQL_GetRowCount(H_Hndl) != 0) {
			if (SQL_FetchRow(H_Hndl)) {
				int I_UId = SQL_FetchInt(H_Hndl, 0);

				Format(C_buffer, 512, "UPDATE `t_players` SET lastname = '%s', lastconnect = '0' WHERE uid = '%i';", C_FormattedName, I_UId);
				gA_SqlQueries.PushString(C_buffer);

				if (I_UId > gA_Names.Length - 1) {
					gA_Names.Resize(I_UId + 1);
					gA_SteamIds.Resize(I_UId + 1);
				}

				gA_Names.SetString(I_UId, C_FormattedName);
				gA_SteamIds.SetString(I_UId, gC_TimerPlayerSteamId[I_Client]);

				gI_TimerPlayerUid[I_Client] = I_UId;

				sql_LoadPlayerData(I_Client);
				sql_LoadPlayerRecords(I_Client);
			}
		} else {
			int I_TimeStamp = GetTime();

			Format(C_buffer, 512, "INSERT INTO `t_players` VALUES ('', '%s', '%s', 'US', '%i', '0', '0', '0', '0', '0');", gC_TimerPlayerSteamId[I_Client], C_FormattedName, I_TimeStamp);
			SQL_TQuery(gD_Main, sqlInsertPlayerUidCallback, C_buffer, GetClientUserId(I_Client), DBPrio_High);
		}
	}
}

public void sqlInsertPlayerUidCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (InsertPlayerUidCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		gB_TimerPlayerNew[I_Client] = true;
		sql_TryLoadPlayerUid(I_Client);
	}
}

public void sqlCalculatePlayerEloCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, DataPack D_Pack) {
	if (H_Hndl == null) {
		LogError("%s (CalculatePlayerEloCallback) - %s", SQL_PREFIX, C_Error);
		return;
	}

	int I_Client, I_UserId, I_UId, I_Style;
	char C_buffer[512], C_MapName[64];

	D_Pack.Reset();

	I_UserId = D_Pack.ReadCell();
	I_UId = D_Pack.ReadCell();
	I_Style = D_Pack.ReadCell();

	I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		Transaction T_Trans = SQL_CreateTransaction();
		while (SQL_FetchRow(H_Hndl)) {
			SQL_FetchString(H_Hndl, 0, C_MapName, 64);

			Format(C_buffer, 512, "SELECT * FROM `t_mapinfo` WHERE mapname = '%s'", C_MapName);
			T_Trans.AddQuery(C_buffer);

			Format(C_buffer, 512, "SELECT FIND_IN_SET(time, (SELECT GROUP_CONCAT(time ORDER BY time ASC) FROM `t_records` WHERE style = '%i' AND mapname = '%s')) AS rank FROM `t_records` WHERE uid = '%i' AND mapname = '%s' AND style = '%i';", I_Style, C_MapName, I_UId, C_MapName, I_Style);
			T_Trans.AddQuery(C_buffer);
		}

		SQL_ExecuteTransaction(gD_Slave, T_Trans, sqlCalculatePlayerEloSuccess, sqlCalculatePlayerEloError, D_Pack, DBPrio_Normal);
	} else {
		CloseHandle(D_Pack);
	}
}

/*
	Format(C_buffer, 512, "SELECT FIND_IN_SET(value, (SELECT GROUP_CONCAT(value) FROM `t_cache` WHERE value != 0 AND style = '%i')) AS rank FROM t_cache WHERE uid = '%i'", I_Style, I_UId);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT value FROM `t_cache` WHERE uid = '%i' AND style = '%i'", I_UId, I_Style);
	T_Trans.AddQuery(C_buffer);

	Format(C_buffer, 512, "SELECT COUNT(*) FROM `t_cache` WHERE total != 0 AND style = '%i'", I_Style);
	T_Trans.AddQuery(C_buffer);
*/

public void sqlCalculatePlayerEloSuccess(Database D_Database, DataPack D_Pack, int I_Queries, Handle[] H_Results, any[] A_QueryData) {
	int I_Client, I_UserId, I_UId, I_Style, I_Type, I_MapTier, I_Rank, I_ArrayCount;
	float F_Total, F_Power, F_Temp[2];
	char C_buffer[512], C_MapName[64];

	D_Pack.Reset();

	I_UserId = D_Pack.ReadCell();
	I_UId = D_Pack.ReadCell();
	I_Style = D_Pack.ReadCell();
	I_Type = D_Pack.ReadCell();

	I_Client = GetClientOfUserId(I_UserId);

	F_Power = Pow(100.0, (1.0 / 1.1));

	float[] F_Points = new float[(I_Queries / 2)];
	float[] F_Weighted = new float[(I_Queries / 2)];
	char[][] C_PointsName = new char[1028][64];

	for (int i = 0; i < I_Queries; i = i + 2) {
		if (SQL_FetchRow(H_Results[i])) {
			SQL_FetchString(H_Results[i], 0, C_MapName, 64);
			I_MapTier = SQL_FetchInt(H_Results[i], 2);
		}

		if (SQL_FetchRow(H_Results[i + 1])) {
			I_Rank = SQL_FetchInt(H_Results[i + 1], 0);
		}

		float F_MapTotal = ((1.0 + (1.0 / float(I_Rank))) / (1.0 / float(I_MapTier)) * F_Power);

		F_Points[I_ArrayCount] = F_MapTotal;
		I_ArrayCount++;
	}

	if (I_Type == 0) {
		SortFloats(F_Points, I_ArrayCount, Sort_Descending);
	} else {
		for (int I_Point = 0; I_Point < (I_Queries / 2); I_Point++) {
			for (int I_Check = I_Point; I_Check > 0; I_Check--) {
				float F_Holder;
				char C_Temp[64];

				if (F_Points[I_Check] > F_Points[I_Check - 1]) {
					Format(C_Temp, 64, C_PointsName[I_Check - 1]);
					F_Holder = F_Points[I_Check - 1];

					Format(C_PointsName[I_Check - 1], 64, C_PointsName[I_Check]);
					F_Points[I_Check - 1] = F_Points[I_Check];

					Format(C_PointsName[I_Check], 64, C_Temp);
					F_Points[I_Check] = F_Holder;
				}
			}
		}
	}

	for (int i = 0; i < (I_Queries / 2); i++) {
		F_Temp[0] = Pow(0.95, float(i));
		F_Temp[1] = (F_Temp[0] * F_Points[i]);

		F_Weighted[i] = F_Temp[1];
		F_Total = F_Total + F_Temp[1];
	}

	if (IsValidClient(I_Client)) {
		if (gI_TimerPlayerUid[I_Client] == I_UId) {
			gF_TimerPlayerEloValue[I_Client][I_Style] = F_Total;

			sql_CalculatePlayerEloRank(I_Client, I_Style);
		}
	}

	Format(C_buffer, 512, "UPDATE `t_cache` SET value = '%f' WHERE uid = '%i' AND style = '%i'", F_Total, I_UId, I_Style);
	gA_SqlQueries.PushString(C_buffer);

	CloseHandle(D_Pack);
}

public void sqlCalculatePlayerEloError(Database D_Database, any A_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] A_QueryData) {
	LogError("%s (CalculatePlayerEloError) - %s", SQL_PREFIX, C_Error);
}
