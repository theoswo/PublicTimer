public void admin_Menu(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminHandle);

	Format(C_buffer, 512, "TangoTimer - Admin Menu\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Zone Management\n");
	Format(C_buffer, 512, "%s > Anything to do with Zoning", C_buffer);

	if (misc_IsWhitelisted(I_Client, 0)) {
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		Format(C_buffer, 512, "%s - Not Whitelisted", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	}

	Format(C_buffer, 512, "Time Management\n");
	Format(C_buffer, 512, "%s > Anything to do with Times", C_buffer);

	if (misc_IsWhitelisted(I_Client, 1)) {
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		Format(C_buffer, 512, "%s - Not Whitelisted", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	}

	Format(C_buffer, 512, "Player Management\n");
	Format(C_buffer, 512, "%s > Anything to do with Players", C_buffer);

	if (misc_IsWhitelisted(I_Client, 2)) {
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		Format(C_buffer, 512, "%s - Not Whitelisted", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	}

	M_Menu.Display(I_Client, 0);
}

public int AdminHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				admin_ZonesMenu(I_Param1);
			} case 1: {
				//admin_TimesMenu(I_Param1);
			} case 2: {
				//admin_PlayersMenu(I_Param1);
			}
		}
	}
}

public void admin_ZonesMenu(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminZoneHandle);

	Format(C_buffer, 512, "TangoTimer - Admin Menu - Zoning\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Add New Zone");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Edit Zones");
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	Format(C_buffer, 512, "Remove Zone Group");
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminZoneHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_TryNukeData(I_Param1);

		admin_Menu(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				gH_AddZonesTimer[I_Param1] = CreateTimer(0.1, Timer_AddZones, I_Param1, TIMER_REPEAT);
			} case 1: {

			} case 2: {

			}
		}
	}
}

public void admin_AddZoneMenu(int I_Client) {
	if (IsValidClient(I_Client)) {
		char[] C_buffer = new char[512];
		Menu M_Menu = new Menu(AdminAddZonesHandle);
		float F_Pos[3], F_ClientEye[3], F_ClientAngles[3];

		GetClientEyeAngles(I_Client, F_ClientAngles);
		GetClientEyePosition(I_Client, F_ClientEye);

		TR_TraceRayFilter(F_ClientEye, F_ClientAngles, MASK_SOLID, RayType_Infinite, HitSelf, I_Client);

		if (TR_DidHit(INVALID_HANDLE)) {
			TR_GetEndPosition(F_Pos);
		}

		Format(C_buffer, 512, "TangoTimer - Admin Menu - Zoning - Add Zone\n");
		Format(C_buffer, 512, "%s \n", C_buffer);

		if (gI_TimerMiscMultipleSettings[I_Client][0] < 2) {
			Format(C_buffer, 512, "%sPos: x: %.2f, y: %.2f, z: %.2f\n", C_buffer, F_Pos[0], F_Pos[1], F_Pos[2]);
			Format(C_buffer, 512, "%s \n", C_buffer);
		} else {
			Format(C_buffer, 512, "%sPointA - Pos: x: %.2f, y: %.2f, z: %.2f\n", C_buffer, gF_AddZonesTemp[I_Client][0][0], gF_AddZonesTemp[I_Client][0][1], gF_AddZonesTemp[I_Client][0][2]);
			Format(C_buffer, 512, "%sPointB - Pos: x: %.2f, y: %.2f, z: %.2f\n", C_buffer, gF_AddZonesTemp[I_Client][1][0], gF_AddZonesTemp[I_Client][1][1], gF_AddZonesTemp[I_Client][1][2]);
			Format(C_buffer, 512, "%s \n", C_buffer);
		}

		M_Menu.SetTitle(C_buffer);

		TR_TraceRayFilter(F_ClientEye, F_ClientAngles, MASK_SOLID, RayType_Infinite, HitSelf, I_Client);

		if (TR_DidHit(INVALID_HANDLE)) {
			TR_GetEndPosition(F_Pos);
		}

		switch (gI_TimerMiscMultipleSettings[I_Client][0]) {
			case -1: {
				Format(C_buffer, 512, "Save First Corner");
				M_Menu.AddItem(C_buffer, C_buffer);

				TE_SetupGlowSprite(F_Pos, gH_Models[BlueGlowSprite], 0.1, 0.1, 249);
				TE_SendToClient(I_Client);
			} case 0: {
				Format(C_buffer, 512, "Save Second Corner");
				M_Menu.AddItem(C_buffer, C_buffer);

				TE_SetupGlowSprite(F_Pos, gH_Models[BlueGlowSprite], 0.1, 0.1, 249);
				TE_SendToClient(I_Client);

				if (F_Pos[2] == gF_AddZonesTemp[I_Client][0][2]) {
					F_Pos[2] += 150.0;
				}

				TE_SetupGlowSprite(gF_AddZonesTemp[I_Client][0], gH_Models[BlueGlowSprite], 0.1, 1.0, 249);
				TE_SendToClient(I_Client);

				zone_ZoneDraw(I_Client, F_Pos, gF_AddZonesTemp[I_Client][0], 6, 0.1, false);
			} case 1: {
				Format(C_buffer, 512, "Edit Second Corner\n");
				Format(C_buffer, 512, "%s \n", C_buffer);
				M_Menu.AddItem(C_buffer, C_buffer);

				Format(C_buffer, 512, "Save Zone");
				M_Menu.AddItem(C_buffer, C_buffer);

				TE_SetupGlowSprite(gF_AddZonesTemp[I_Client][0], gH_Models[BlueGlowSprite], 0.1, 1.0, 249);
				TE_SendToClient(I_Client);

				TE_SetupGlowSprite(gF_AddZonesTemp[I_Client][1], gH_Models[BlueGlowSprite], 0.1, 1.0, 249);
				TE_SendToClient(I_Client);

				zone_ZoneDraw(I_Client, gF_AddZonesTemp[I_Client][0], gF_AddZonesTemp[I_Client][1], 6, 0.1, false);
			}
		}

		M_Menu.ExitBackButton = true;
		M_Menu.Display(I_Client, 1);
	}
}

public int AdminAddZonesHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_TryNukeData(I_Param1);

		admin_ZonesMenu(I_Param1);
		return;
	}

	if (I_Param2 == MenuCancel_Disconnected || I_Param2 == MenuCancel_Exit) {
		admin_TryNukeData(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		float F_Pos[3], F_ClientEye[3], F_ClientAngles[3];

		GetClientEyeAngles(I_Param1, F_ClientAngles);
		GetClientEyePosition(I_Param1, F_ClientEye);

		TR_TraceRayFilter(F_ClientEye, F_ClientAngles, MASK_SOLID, RayType_Infinite, HitSelf, I_Param1);

		if (TR_DidHit(INVALID_HANDLE)) {
			TR_GetEndPosition(F_Pos);
		}

		switch (gI_TimerMiscMultipleSettings[I_Param1][0]) {
			case -1: {
				switch (I_Param2) {
					case 0: {
						if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
							PrintToChat(I_Param1, "%s First Corner Saved", PLUGIN_PREFIX);
						}

						for (int i = 0; i < 3; i++) {
							gF_AddZonesTemp[I_Param1][0][i] = F_Pos[i];
						}

						gI_TimerMiscMultipleSettings[I_Param1][0] = 0;
					}
				}
			} case 0: {
				switch (I_Param2) {
					case 0 : {
						if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
							PrintToChat(I_Param1, "%s Second Corner Saved", PLUGIN_PREFIX);
						}

						if (F_Pos[2] == gF_AddZonesTemp[I_Param1][0][2]) {
							F_Pos[2] += 150.0;
						}

						for (int i = 0; i < 3; i++) {
							gF_AddZonesTemp[I_Param1][1][i] = F_Pos[i];
						}

						gI_TimerMiscMultipleSettings[I_Param1][0] = 1;
					}
				}
			} case 1: {
				switch (I_Param2) {
					case 0: {
						if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
							PrintToChat(I_Param1, "%s Editing Second Corner", PLUGIN_PREFIX);
						}

						gI_TimerMiscMultipleSettings[I_Param1][0] = 0;
					} case 1: {
						if (gI_TimerPlayerSettings[I_Param1][3] == 1) {
							PrintToChat(I_Param1, "%s Saving Zone", PLUGIN_PREFIX);
						}

						admin_NukeTimer(I_Param1);
						admin_PreSaveZone(I_Param1);
					}
				}
			}
		}
	}
}

public void admin_PreSaveZone(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminPreSaveZoneHandle);

	Format(C_buffer, 512, "TangoTimer - Admin Menu - Zoning - Save Zone\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sSelect ZoneGroup for Zone\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Add New Zone Group");

	if (gI_TotalZoneGroups + 1 > 64) {
		Format(C_buffer, 512, "%s - Limit Reached", C_buffer);
		Format(C_buffer, 512, "%s \n", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		Format(C_buffer, 512, "%s \n", C_buffer);
		Format(C_buffer, 512, "%s \n", C_buffer);
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	for (int i = gI_TotalZoneGroups - 1; gI_TotalZoneGroups > 0; i--) {
		Format(C_buffer, 512, "Zone Group: %i", i);
		M_Menu.AddItem(C_buffer, C_buffer);

		if (i == 0) {
			break;
		}
	}

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminPreSaveZoneHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_TryNukeData(I_Param1);

		admin_ZonesMenu(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		if (I_Param2 == 0) {
			gI_TotalZoneGroups++;
			gI_TimerMiscMultipleSettings[I_Param1][1] = gI_TotalZoneGroups - 1;
		} else {
			gI_TimerMiscMultipleSettings[I_Param1][1] = gI_TotalZoneGroups - I_Param2;
		}

		admin_SaveZone(I_Param1);
	}
}

public void admin_SaveZone(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminSaveZoneHandle);

	Format(C_buffer, 512, "TangoTimer - Admin Menu - Zoning - Save Zone\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sZoneGroup: %i\n", C_buffer, gI_TimerMiscMultipleSettings[I_Client][1]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sSelect ZoneType for Zone\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Start Zone");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "End Zone");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Checkpoint (%i)", gI_CheckpointTotalCount[gI_TimerMiscMultipleSettings[I_Client][1]] + 1);
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminSaveZoneHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	int I_ZoneType;

	if (I_Param2 == MenuCancel_ExitBack) {
		admin_TryNukeData(I_Param1);

		admin_ZonesMenu(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		I_ZoneType = I_Param2;

		if (gI_TimerMiscMultipleSettings[I_Param1][1] > 0) {
			I_ZoneType += 3;
		}

		if (I_ZoneType == 2 || I_ZoneType == 5) {
			sql_InsertZone(I_Param1, I_ZoneType, gI_TimerMiscMultipleSettings[I_Param1][1], gF_AddZonesTemp[I_Param1][0], gF_AddZonesTemp[I_Param1][1]);
		} else {
			sql_CheckZone(I_Param1, I_ZoneType, gI_TimerMiscMultipleSettings[I_Param1][1], gF_AddZonesTemp[I_Param1][0], gF_AddZonesTemp[I_Param1][1])
		}

		if (I_ZoneType == 1 || I_ZoneType == 4) {
			admin_MapInfo(I_Param1);
		}
	}
}

public void admin_MapInfo(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminMapInfoHandle);

	Format(C_buffer, 512, "TangoTimer - Admin Menu - Zoning - Map Info\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Zone Group Type: %s\n", ((gI_TimerMiscMultipleSettings[I_Client][2] > -1) ? "Linear" : "Staged"));
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	for (int i = 0; i < 6; i++) {
		Format(C_buffer, 512, "Tier: %i", i + 1);
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	M_Menu.Display(I_Client, 0);
}

public int AdminMapInfoHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		if (I_Param2 == 0) {
			if (gI_TimerMiscMultipleSettings[I_Param1][2] > -1) {
				gI_TimerMiscMultipleSettings[I_Param1][2] = -1;
			} else {
				gI_TimerMiscMultipleSettings[I_Param1][2]++;
			}

			if (gI_TimerPlayerSettings[I_Param1][3]) {
				PrintToChat(I_Param1, "%s Zone Group Type: \x10%s", PLUGIN_PREFIX, ((gI_TimerMiscMultipleSettings[I_Param1][2] > -1) ? "Linear" : "Staged"));
			}

			admin_MapInfo(I_Param1);
		} else {
			gI_TimerMiscMultipleSettings[I_Param1][2]++;

			sql_CheckMapInfo(I_Param1, gI_TimerMiscMultipleSettings[I_Param1][1], gI_TimerMiscMultipleSettings[I_Param1][2], I_Param2);
		}
	}
}

public void admin_TryNukeData(int I_Client) {
	admin_NukeTimer(I_Client);

	for (int i = 0; i < 2; i++) {
		for (int x = 0; x < 3; x++) {
			gF_AddZonesTemp[I_Client][i][x] = 0.0;
		}
	}
}

public void admin_NukeTimer(int I_Client) {
	if (gH_AddZonesTimer[I_Client] != INVALID_HANDLE) {
		CloseHandle(gH_AddZonesTimer[I_Client]);
		gH_AddZonesTimer[I_Client] = INVALID_HANDLE
	}
}

public bool HitSelf(int entity, int contentsMask, any data) {
	if (entity == data) {
		return false;
	}

	return true;
}
